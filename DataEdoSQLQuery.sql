DECLARE @date date= '2021-04-01';

SELECT i.id, c.customer_name, cities.name AS City, i.segment, i.cost, i.order_date, i.installation_date, 
	(DATEDIFF( day, i.order_date, COALESCE(i.installation_date, @date)) - 90) * (i.cost * 0.05) AS Penalty
FROM installations AS i
	JOIN customers AS c ON i.customer_id = c.id
	JOIN cities ON i.city_id = cities.id
WHERE i.segment = 'A'
	AND DATEDIFF (day, i.order_date, COALESCE(i.installation_date, @date)) > 90

UNION

SELECT i.id, c.customer_name, cities.name AS City, i.segment, i.cost, i.order_date, i.installation_date, 
	(DATEDIFF( day, i.order_date, COALESCE(i.installation_date, @date)) - 60) * (i.cost * 0.1) AS Penalty
FROM installations AS i
	JOIN customers AS c ON i.customer_id = c.id
	JOIN cities ON i.city_id = cities.id
WHERE i.segment = 'B'
	AND DATEDIFF (day, i.order_date, COALESCE(i.installation_date, @date)) > 60

UNION

SELECT i.id, c.customer_name, cities.name AS City, i.segment, i.cost, i.order_date, i.installation_date, 
	(DATEDIFF( day, i.order_date, COALESCE(i.installation_date, @date)) - 30) * (i.cost * 0.15) AS Penalty
FROM installations AS i
	JOIN customers AS c ON i.customer_id = c.id
	JOIN cities ON i.city_id = cities.id
WHERE i.segment = 'C'
	AND DATEDIFF (day, i.order_date, COALESCE(i.installation_date, @date)) > 30